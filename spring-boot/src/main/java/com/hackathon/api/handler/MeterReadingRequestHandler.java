package com.hackathon.api.handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hackathon.api.model.Request;
import com.hackathon.api.model.Response;
import com.hackathon.api.service.MeterReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * Our Lambda function's main logic takes place here, while we
 * leverage Spring's dependency injection features to inject the
 * services we need at construction time.
 *
 * This class is declared as a bean using the {@link Component}
 * annotation. You could also just as easily register it in the
 * {@link com.apiture.api.ApplicationConfiguration} class, or
 * in a Spring application configuration XML file.
 *
 * @author Chris Campo
 */
@Component
public class MeterReadingRequestHandler implements RequestHandler<Request, Response> {

    private final MeterReadingService meterReadingService;

    /**
     * Dependency injection is handled via autowiring!
     */
    @Autowired
    public MeterReadingRequestHandler(final MeterReadingService meterReadingService) {
        this.meterReadingService = Objects.requireNonNull(meterReadingService, "meterReadingService");
    }

    @Override
    public Response handleRequest(final Request input, final Context context) {
        final String responseMessage = "Request message: " + input.getMessage()
                + ", Service A message: " + meterReadingService.getMessage();
        final Response response = new Response();
        response.setBody(responseMessage);
        response.setStatusCode(200);
        return response;
    }
}
