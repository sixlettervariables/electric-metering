package com.hackathon.api.model;

/**
 * @author Chris Campo
 */
public class Response {

    public enum Status {
        OK, ERROR;
    }

    private String body;
    private Integer statusCode;
    private Boolean isBase64Encoded = false;

    public Integer getStatusCode() {
        return statusCode;
        
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
        
    }
    
    public void setBody(String body) {
    		this.body = body;
    		
    }

}
