package com.hackathon.api;

import com.hackathon.api.service.MeterReadingService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Declare all your beans here, or use the annotation driven approach
 * with {@link org.springframework.context.annotation.ComponentScan}.
 *
 */
@Configuration
@ComponentScan
public class ApplicationConfiguration {

    @Bean
    public MeterReadingService meterReadingService() {
        return new MeterReadingService();
    }
}
