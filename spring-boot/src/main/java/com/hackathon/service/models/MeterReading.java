package com.hackathon.service.models;

import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

public class MeterReading {

	@Getter
	@Setter
	private Long ID;
	
	@Getter
	@Setter
	private BigDecimal Value;
	
	@Getter
	@Setter
	private String Status;
	
	@Getter
	@Setter
	private String Meter_Name;
	
	@Getter
	@Setter
	private String Status_Tag;
	
	@Getter
	@Setter
	private String Trend_Flag;

	@Getter
	@Setter
	private String Trend_Flag_Tag;

	@Getter
	@Setter
	private Timestamp TimeStamp;

	private MeterReading() {

	}

	@Override
	public String toString() {
		return "Meter_Name: " + Meter_Name + " Status: " + Status;
		
	}
}
