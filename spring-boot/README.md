# GE-Hitachi Electric Meter Project


Configure your AWS credentials.

Create a file HOME_DIR/.aws/credentials

See slack for client and secret key.

* Clone the repository
```
https://timothy_batchelor@bitbucket.org/timothy_batchelor/electric-metering.git
```

* Deploy to AWS

```
serverless deploy
```
