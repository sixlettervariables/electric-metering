import json
import pandas as pd
import boto3

s3 = boto3.client('s3')

def postEMData(event, context):
	fileKey = pd.to_datetime + 'ge-em-project'
	df = pd.read_json(json.dumps(body),orient='index')
	with open('filename', 'rb') as data:
    		s3.upload_fileobj(df.to_csv(), 'ge-em-project', fileKey)
    
	body = {
	    "message": "Your function executed successfully!",
	    "input": event
	}
	
	response = {
	    "statusCode": 200,
	    "body": json.dumps(body)
	}
	
	return response
